package pl.sda.rav_project.dao;

import org.junit.jupiter.api.Test;
import pl.sda.rav_project.domain.*;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VehiclesDaoTest {
    @Test
    void shouldAddVehicles() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now(), 700, 100, 10);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now(), 1000, 20);

        //when
        vehiclesDao.add(car);
        vehiclesDao.add(amphibian);
        vehiclesDao.add(motorboat);

        //then
        List<Vehicle> all = vehiclesDao.getAll();
        assertEquals(3, all.size());
        assertEquals("CAR-123", all.get(0).getVin());
        assertEquals("AMV-345", all.get(1).getVin());
        assertEquals("MOTORB-567", all.get(2).getVin());
    }

    @Test
    void shouldRemoveVehicles() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now(), 700, 100, 10);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now(), 1000, 20);

        //when
        vehiclesDao.add(car);
        vehiclesDao.add(amphibian);
        vehiclesDao.add(motorboat);
        assertEquals(3, vehiclesDao.getAll().size());
        vehiclesDao.delete(amphibian.getVin());

        //then
        assertEquals(2, vehiclesDao.getAll().size());
    }

    @Test
    void shouldNotRemoveNotExistingVin() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now(), 700, 100, 10);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now(), 1000, 20);

        //when
        vehiclesDao.add(car);
        vehiclesDao.add(amphibian);
        vehiclesDao.add(motorboat);
        vehiclesDao.delete("not_existing_vin");

        //then
        assertEquals(3, vehiclesDao.getAll().size());
    }

    @Test
    void shouldSearchWithoutParams() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        vehiclesDao.add(car);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now(), 700, 100, 10);
        vehiclesDao.add(amphibian);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now(), 1000, 20);
        vehiclesDao.add(motorboat);

        //when
        List<Vehicle> vehicleList = vehiclesDao.search(new SearchParams());

        //then
        assertEquals(3, vehicleList.size());
    }

    @Test
    void shouldSearchByCategory() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        vehiclesDao.add(car);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now(), 700, 100, 10);
        vehiclesDao.add(amphibian);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now(), 1000, 20);
        vehiclesDao.add(motorboat);

        //when
        SearchParams searchParams = new SearchParams(VehicleCategory.DRIVING, null, null);
        List<Vehicle> vehicleList = vehiclesDao.search(searchParams);

        //then
        assertEquals(2, vehicleList.size());
        assertEquals("CAR-123", vehicleList.get(0).getVin());
        assertEquals("AMV-345", vehicleList.get(1).getVin());
    }

    @Test
    void shouldSearchByCategoryAndStatus() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        vehiclesDao.add(car);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now(), 700, 100, 10);
        vehiclesDao.add(amphibian);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now(), 1000, 20);
        vehiclesDao.add(motorboat);
        motorboat = new Motorboat("MOTORB-113", Vehicle.Status.LENT, "Moto-Two", LocalDate.now(), 1000, 20);
        vehiclesDao.add(motorboat);

        //when
        SearchParams searchParams = new SearchParams(VehicleCategory.FLOATING, Vehicle.Status.AVAILABLE, null);
        List<Vehicle> vehicleList = vehiclesDao.search(searchParams);

        //then
        assertEquals(1, vehicleList.size());
        assertEquals("AMV-345", vehicleList.get(0).getVin());
    }

    @Test
    void shouldSearchByAge() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        vehiclesDao.add(car);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now().minusYears(1), 700, 100, 10);
        vehiclesDao.add(amphibian);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now().minusYears(3), 1000, 20);
        vehiclesDao.add(motorboat);
        motorboat = new Motorboat("MOTORB-113", Vehicle.Status.LENT, "Moto-Two", LocalDate.now().minusYears(20), 1000, 20);
        vehiclesDao.add(motorboat);

        //when
        SearchParams searchParams = new SearchParams(null, null, VehicleAge.YOUNG);
        List<Vehicle> youngList = vehiclesDao.search(searchParams);
        searchParams = new SearchParams(null, null, VehicleAge.MIDDLE);
        List<Vehicle> middleList = vehiclesDao.search(searchParams);
        searchParams = new SearchParams(null, null, VehicleAge.OLD);
        List<Vehicle> oldList = vehiclesDao.search(searchParams);

        //then
        assertEquals(2, youngList.size());
        assertEquals("CAR-123", youngList.get(0).getVin());
        assertEquals("AMV-345", youngList.get(1).getVin());

        assertEquals(1, middleList.size());
        assertEquals("MOTORB-567", middleList.get(0).getVin());

        assertEquals(1, oldList.size());
        assertEquals("MOTORB-113", oldList.get(0).getVin());
    }

    @Test
    void shouldSearchByBodyType() {
        //having
        VehiclesDao vehiclesDao = new VehiclesDao();

        Car car = new Car("CAR-123", Vehicle.Status.AVAILABLE, "Toyota Avensis", LocalDate.now(), 1000, BodyType.WAGON);
        vehiclesDao.add(car);
        car = new Car("CAR-555", Vehicle.Status.AVAILABLE, "Toyota Yaris", LocalDate.now(), 1000, BodyType.SEDAN);
        vehiclesDao.add(car);
        Amphibian amphibian = new Amphibian("AMV-345", Vehicle.Status.AVAILABLE, "Amp-Lux-One", LocalDate.now().minusYears(1), 700, 100, 10);
        vehiclesDao.add(amphibian);
        amphibian = new Amphibian("AMV-1009", Vehicle.Status.AVAILABLE, "Amp-Lux-Second", LocalDate.now().minusYears(11), 700, 100, 10);
        vehiclesDao.add(amphibian);
        Motorboat motorboat = new Motorboat("MOTORB-567", Vehicle.Status.IN_REPAIR, "Moto-One", LocalDate.now().minusYears(3), 1000, 20);
        vehiclesDao.add(motorboat);
        motorboat = new Motorboat("MOTORB-113", Vehicle.Status.LENT, "Moto-Two", LocalDate.now().minusYears(20), 1000, 20);
        vehiclesDao.add(motorboat);

        //when
        SearchParams searchParams = new DrivingSearchParams(null, null, null, BodyType.SEDAN);
        List<Vehicle> sedanList = vehiclesDao.search(searchParams);
        searchParams = new DrivingSearchParams(null, null, null, BodyType.AMPHIBIAN);
        List<Vehicle> amphibianList = vehiclesDao.search(searchParams);

        //then
        assertEquals(1, sedanList.size());
        assertEquals("CAR-555", sedanList.get(0).getVin());

        assertEquals(2, amphibianList.size());
        assertEquals("AMV-345", amphibianList.get(0).getVin());
        assertEquals("AMV-1009", amphibianList.get(1).getVin());
    }
}