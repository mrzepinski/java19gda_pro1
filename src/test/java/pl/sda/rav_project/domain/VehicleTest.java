package pl.sda.rav_project.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class VehicleTest {

    @Test
    void shouldCheckAvailability() {
        //having
        VehicleToTest availableVehicle = new VehicleToTest("V123", Vehicle.Status.AVAILABLE, "v1", LocalDate.now());
        VehicleToTest lentVehicle = new VehicleToTest("V124", Vehicle.Status.LENT, "v2", LocalDate.now());
        VehicleToTest repairedVehicle = new VehicleToTest("V125", Vehicle.Status.IN_REPAIR, "v3", LocalDate.now());

        //when
        //then
        assertTrue(availableVehicle.isAvailable());
        assertFalse(lentVehicle.isAvailable());
        assertFalse(repairedVehicle.isAvailable());
    }

    @Test
    void shouldReturnAge() {
        //having
        VehicleToTest oneYearOldVehicle = new VehicleToTest("V123", Vehicle.Status.AVAILABLE, "v1", LocalDate.now().minusYears(1));
        VehicleToTest fiveYearOldVehicle = new VehicleToTest("V124", Vehicle.Status.LENT, "v2", LocalDate.now().minusYears(5));
        VehicleToTest thisYearOldVehicle = new VehicleToTest("V125", Vehicle.Status.IN_REPAIR, "v3", LocalDate.now());

        //when
        //then
        assertEquals(1, oneYearOldVehicle.getAge());
        assertEquals(5, fiveYearOldVehicle.getAge());
        assertEquals(0, thisYearOldVehicle.getAge());
    }
}

class VehicleToTest extends Vehicle {
    VehicleToTest(String vin, Status status, String name, LocalDate productionDate) {
        super(vin, status, name, productionDate);
    }
}