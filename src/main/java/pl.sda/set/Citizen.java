package pl.sda.set;

public class Citizen implements Comparable<Citizen> {
    private final String name;
    private final String surname;
    private final String pesel;

    public Citizen(String name, String surname, String pesel) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPesel() {
        return pesel;
    }

    @Override
    public String toString() {
        return String.format("%s %s (pesel:%s)",name, surname,  pesel);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Citizen citizen = (Citizen) o;

        return pesel != null ? pesel.equals(citizen.pesel) : citizen.pesel == null;
    }

    @Override
    public int hashCode() {
        return pesel != null ? pesel.hashCode() : 0;
    }

    @Override
    public int compareTo(Citizen that) {
        if(that == null) {
            return 1;
        }

        int surnameComparing = this.surname.compareTo(that.getSurname());
        if(surnameComparing != 0) {
            return surnameComparing;
        }

        return this.name.compareTo(that.getName());
    }
}