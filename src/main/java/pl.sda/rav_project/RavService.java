package pl.sda.rav_project;

import pl.sda.rav_project.dao.OrdersDao;
import pl.sda.rav_project.dao.SearchParams;
import pl.sda.rav_project.dao.UsersDao;
import pl.sda.rav_project.dao.VehiclesDao;
import pl.sda.rav_project.domain.User;
import pl.sda.rav_project.domain.Vehicle;

import java.util.List;
import java.util.Optional;

public class RavService {
    private UsersDao usersDao;
    private VehiclesDao vehiclesDao;
    private OrdersDao ordersDao;

    private User currentUser;

    public boolean login(String login, String password) {
        Optional<User> optionalUser = usersDao.getByLoginAndPassword(login, password);
        if(!optionalUser.isPresent()) {
            return false;
        }

        currentUser = optionalUser.get();
        return true;
    }

    public List<Vehicle> searchVehicle(SearchParams searchParams) {
        if(currentUser == null) {
            throw new IllegalStateException("User not log-in");
        }

        return vehiclesDao.search(searchParams);
    }
}
