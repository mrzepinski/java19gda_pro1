package pl.sda.rav_project.domain;

public class User {
    private String login;
    private String password;
    private Type type;

    enum Type {CUSTOMER, ADMIN}

    public User(String login, String password, Type type) {
        this.login = login;
        this.password = password;
        this.type = type;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", type=" + type +
                '}';
    }
}
