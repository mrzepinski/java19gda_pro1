package pl.sda.rav_project.domain;

import java.time.LocalDate;

public class Amphibian extends Vehicle implements Driving, Floating {
    private int maxDistanceKm;
    private int maxDistanceMiles;
    private int displacement;

    public Amphibian(String vin, Status status, String name, LocalDate productionDate, int maxDistanceKm, int maxDistanceMiles, int displacement) {
        super(vin, status, name, productionDate);
        this.maxDistanceKm = maxDistanceKm;
        this.maxDistanceMiles = maxDistanceMiles;
        this.displacement = displacement;
    }

    public int getMaxDistanceKm() {
        return maxDistanceKm;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.AMPHIBIAN;
    }

    public int getMaxDistanceMiles() {
        return maxDistanceMiles;
    }

    public int getDisplacement() {
        return displacement;
    }

    @Override
    public String toString() {
        return "Amphibian{" +
                "vin='" + getVin() + '\'' +
                ", status=" + getStatus() +
                ", name='" + getName() + '\'' +
                ", productionDate=" + getProductionDate() +
                ", maxDistanceKm=" + maxDistanceKm +
                ", maxDistanceMiles=" + maxDistanceMiles +
                ", displacement=" + displacement +
                '}';
    }
}
