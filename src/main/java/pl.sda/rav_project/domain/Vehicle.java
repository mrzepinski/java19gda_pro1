package pl.sda.rav_project.domain;

import java.time.LocalDate;
import java.time.Period;

public abstract class Vehicle {
    private String vin;
    private Status status;
    private String name;
    private LocalDate productionDate;

    public enum Status {
        AVAILABLE,
        LENT,
        IN_REPAIR;
    }

    public Vehicle(String vin, Status status, String name, LocalDate productionDate) {
        this.vin = vin;
        this.status = status;
        this.name = name;
        this.productionDate = productionDate;
    }

    public String getVin() {
        return vin;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public boolean isAvailable() {
        return status == Status.AVAILABLE;
    }

    public int getAge() {
        Period period = Period.between(productionDate, LocalDate.now());
        return period.getYears();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;

        return vin != null ? vin.equals(vehicle.vin) : vehicle.vin == null;
    }

    @Override
    public int hashCode() {
        return vin != null ? vin.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vin='" + vin + '\'' +
                ", status=" + status +
                ", name='" + name + '\'' +
                ", productionDate=" + productionDate +
                '}';
    }
}
