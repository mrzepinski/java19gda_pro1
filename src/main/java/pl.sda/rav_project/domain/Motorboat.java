package pl.sda.rav_project.domain;

import java.time.LocalDate;

public class Motorboat extends Vehicle implements Floating{
    private int maxDistance;
    private int displacement;

    public Motorboat(String vin, Status status, String name, LocalDate productionDate, int maxDistance, int displacement) {
        super(vin, status, name, productionDate);
        this.maxDistance = maxDistance;
        this.displacement = displacement;
    }

    @Override
    public int getMaxDistanceMiles() {
        return maxDistance;
    }

    @Override
    public int getDisplacement() {
        return displacement;
    }

    @Override
    public String toString() {
        return "Motorboat{" +
                "vin='" + getVin() + '\'' +
                ", status=" + getStatus() +
                ", name='" + getName() + '\'' +
                ", productionDate=" + getProductionDate() +
                ", maxDistance=" + maxDistance +
                ", displacement=" + displacement +
                '}';
    }
}