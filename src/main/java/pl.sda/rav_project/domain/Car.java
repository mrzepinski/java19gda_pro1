package pl.sda.rav_project.domain;

import java.time.LocalDate;

public class Car extends Vehicle implements Driving{
    private int maxDistance;
    private BodyType bodyType;

    public Car(String vin, Status status, String name, LocalDate productionDate, int maxDistance, BodyType bodyType) {
        super(vin, status, name, productionDate);
        this.maxDistance = maxDistance;
        this.bodyType = bodyType;
    }

    @Override
    public int getMaxDistanceKm() {
        return maxDistance;
    }

    @Override
    public BodyType getBodyType() {
        return bodyType;
    }

    @Override
    public String toString() {
        return "Car{" +
                "vin='" + getVin() + '\'' +
                ", status=" + getStatus() +
                ", name='" + getName() + '\'' +
                ", productionDate=" + getProductionDate() +
                ", maxDistance=" + maxDistance +
                ", bodyType=" + bodyType +
                '}';
    }
}