package pl.sda.rav_project.domain;

public interface Floating {
    int getMaxDistanceMiles();

    int getDisplacement();
}
