package pl.sda.rav_project.domain;

public enum BodyType {
    SEDAN, VAN, SUV, WAGON, AMPHIBIAN
}
