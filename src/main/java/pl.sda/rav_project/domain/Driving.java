package pl.sda.rav_project.domain;

public interface Driving {
    int getMaxDistanceKm();
    BodyType getBodyType();
}
