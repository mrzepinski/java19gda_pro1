package pl.sda.rav_project.dao;

import pl.sda.rav_project.domain.Driving;
import pl.sda.rav_project.domain.Floating;
import pl.sda.rav_project.domain.Vehicle;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VehiclesDao extends AbstractDao<String, Vehicle> {
    public Optional<Vehicle> getById(String id) {
        return getAll().stream()
                .filter(v -> v.getVin().equals(id))
                .findFirst();
    }

    public List<Vehicle> search(SearchParams searchParams) {
        return getAll().stream()
                .filter(vehicle -> checkCategory(searchParams.getCategory(), vehicle))
                .filter(vehicle -> checkStatus(searchParams.getStatus(), vehicle))
                .filter(vehicle -> checkAge(searchParams.getAge(), vehicle))
                .filter(vehicle -> checkBodyType(searchParams, vehicle))
                .collect(Collectors.toList());
    }

    private boolean checkCategory(VehicleCategory category, Vehicle vehicle) {
        if(category == null) {
            return true;
        } else if(category == VehicleCategory.DRIVING) {
            return vehicle instanceof Driving;
        } else if(category == VehicleCategory.FLOATING) {
            return vehicle instanceof Floating;
        }

        return false;
    }

    private boolean checkStatus(Vehicle.Status status, Vehicle vehicle) {
        if(status == null) {
            return true;
        }

        return vehicle.getStatus() == status;
    }

    private boolean checkAge(VehicleAge age, Vehicle vehicle) {
        int vehicleAge = vehicle.getAge();
        if(age == null) {
            return true;
        } else if(age == VehicleAge.YOUNG) {
            return vehicleAge >= 0 && vehicleAge <= 2;
        } else if(age == VehicleAge.MIDDLE) {
            return vehicleAge >= 3 && vehicleAge <= 5;
        } else if(age == VehicleAge.OLD) {
            return vehicleAge > 5;
        }

        return false;
    }

    private boolean checkBodyType(SearchParams searchParams, Vehicle vehicle) {
        if(!(searchParams instanceof DrivingSearchParams)) {
            return true;
        }

        if(!(vehicle instanceof Driving)) {
            return false;
        }

        DrivingSearchParams drivingSearchParams = (DrivingSearchParams) searchParams;
        Driving driving = (Driving) vehicle;
        if(drivingSearchParams.getBodyType() == null) {
            return true;
        }

        return driving.getBodyType() == drivingSearchParams.getBodyType();
    }
}