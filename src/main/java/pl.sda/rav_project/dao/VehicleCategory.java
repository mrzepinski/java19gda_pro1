package pl.sda.rav_project.dao;

public enum  VehicleCategory {
    DRIVING,
    FLOATING,
    FLYING
}
