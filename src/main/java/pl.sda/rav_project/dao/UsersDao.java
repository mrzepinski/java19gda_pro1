package pl.sda.rav_project.dao;

import pl.sda.rav_project.domain.User;

import java.util.Optional;

public class UsersDao extends AbstractDao<String, User> {
    public Optional<User> getById(String id) {
        return getAll().stream()
                .filter(user -> user.getLogin().equals(id))
                .findFirst();
    }

    public Optional<User> getByLoginAndPassword(String login, String password) {
        return getAll().stream()
                .filter(user -> user.getLogin().equals(login))
                .filter(user -> user.getPassword().equals(password))
                .findFirst();
    }
}