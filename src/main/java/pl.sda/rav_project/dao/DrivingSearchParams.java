package pl.sda.rav_project.dao;

import pl.sda.rav_project.domain.BodyType;
import pl.sda.rav_project.domain.Vehicle;

public class DrivingSearchParams extends SearchParams {
    private BodyType bodyType;

    public DrivingSearchParams() {
    }

    public DrivingSearchParams(VehicleCategory category, Vehicle.Status status, VehicleAge age, BodyType bodyType) {
        super(category, status, age);
        this.bodyType = bodyType;
    }

    public BodyType getBodyType() {
        return bodyType;
    }
}