package pl.sda.rav_project.dao;

public enum  VehicleAge {
    YOUNG, MIDDLE, OLD
}
