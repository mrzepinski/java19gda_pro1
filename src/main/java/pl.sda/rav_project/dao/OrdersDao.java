package pl.sda.rav_project.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.rav_project.domain.Order;
import pl.sda.rav_project.domain.User;
import pl.sda.rav_project.domain.Vehicle;

import java.time.LocalDate;
import java.util.*;

public class OrdersDao {
    Logger logger = LoggerFactory.getLogger(getClass());
    private Set<Order> orders = new LinkedHashSet<>();

    public List<Order> getOrders() {
        return new ArrayList<>(orders);
    }

    public boolean order(User user, Vehicle vehicle, LocalDate startDate, LocalDate endDate) {
        if (!vehicle.isAvailable()) {
            logger.warn("Attempt to rent unavailable vehicle! user: {}, vehicle:{}", user, vehicle);
            return false;
        }

        orders.add(new Order(user, vehicle, startDate, endDate));
        vehicle.setStatus(Vehicle.Status.LENT);
        return true;
    }

    public boolean returnVehicle(User user, Vehicle vehicle) {
        Optional<Order> foundOrder = orders.stream()
                .filter(order -> order.getCustomer().equals(user))
                .filter(order -> order.getVehicle().equals(vehicle))
                .findFirst();

        if (foundOrder.isPresent()) {
            Order order = foundOrder.get();
            order.setReturnDate(LocalDate.now());
            vehicle.setStatus(Vehicle.Status.AVAILABLE);
            return true;
        } else {
            logger.warn("Can't find order to return vehicle! user: {}, vehicle:{}", user, vehicle);
            return false;
        }
    }

    public boolean isAvailable(Vehicle vehicle, LocalDate from, LocalDate to) {
        Optional<Order> foundOrder = orders.stream()
                .filter(order -> order.getVehicle().equals(vehicle))
                .filter(order -> order.getStartDate().isBefore(from) && order.getEndDate().isAfter(from) ||
                        order.getStartDate().isBefore(to) && order.getEndDate().isAfter(to) ||
                        order.getStartDate().isAfter(from) && order.getEndDate().isBefore(to)
                        )
                .findFirst();

        return !foundOrder.isPresent();
    }
}