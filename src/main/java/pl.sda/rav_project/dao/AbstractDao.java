package pl.sda.rav_project.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class AbstractDao<T, K> {
    private static Logger logger = LoggerFactory.getLogger(AbstractDao.class);

    private Set<K> items = new LinkedHashSet<>();

    public List<K> getAll() {
        return new ArrayList<>(items);
    }

    public void add(K item) {
        items.add(item);
    }

    public boolean delete(T id) {
        Optional<K> found = getById(id);
        if(!found.isPresent()) {
            logger.warn("Vehicle of id: '{}' not found", id);
            return false;
        }

        items.remove(found.get());
        return true;
    }

    public abstract Optional<K> getById(T id);
}
