package pl.sda.rav_project.dao;

import pl.sda.rav_project.domain.Vehicle;

public class SearchParams {
    private VehicleCategory category;
    private Vehicle.Status status;
    private VehicleAge age;

    public SearchParams() {}

    public SearchParams(VehicleCategory category, Vehicle.Status status, VehicleAge age) {
        this.category = category;
        this.status = status;
        this.age = age;
    }

    public VehicleCategory getCategory() {
        return category;
    }

    public Vehicle.Status getStatus() {
        return status;
    }

    public VehicleAge getAge() {
        return age;
    }
}
