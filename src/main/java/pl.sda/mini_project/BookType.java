package pl.sda.mini_project;

public enum  BookType {
    DETECTIVE_STORY,
    DRAMA,
    HISTORICAL,
    POETRY
}
