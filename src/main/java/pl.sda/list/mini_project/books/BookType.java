package pl.sda.list.mini_project.books;

public enum  BookType {
    DETECTIVE_STORY,
    DRAMA,
    HISTORICAL,
    POETRY
}
